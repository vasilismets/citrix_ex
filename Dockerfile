FROM alpine:3.11
#FROM alpine:3.5
MAINTAINER Vasilios Metaxiotis
RUN apk update
RUN apk add git
RUN apk add build-base
RUN apk add wget
RUN apk add openjdk11
RUN apk --no-cache add zip
#RUN apk --update add openjdk8-jre
#RUN apk --update add openjdk11-jre
#WORKDIR /root
RUN mkdir /home/vmetax
WORKDIR /home/vmetax
RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.4.0.2170-linux.zip
RUN unzip sonar-scanner-cli-4.4.0.2170-linux.zip
RUN rm -fr sonar-scanner-cli-4.4.0.2170-linux.zip
RUN sed -i 's/localhost/192.168.105.117/g' /home/vmetax/sonar-scanner-4.4.0.2170-linux/conf/sonar-scanner.properties
RUN sed -i 's/#sonar.host.url=/sonar.host.url=/g' /home/vmetax/sonar-scanner-4.4.0.2170-linux/conf/sonar-scanner.properties
RUN sed -i 's/#sonar.sourceEncoding=UTF-8/sonar.sourceEncoding=UTF-8/g' /home/vmetax/sonar-scanner-4.4.0.2170-linux/conf/sonar-scanner.properties
RUN sed -i 's/use_embedded_jre=true/use_embedded_jre=false/g' /home/vmetax/sonar-scanner-4.4.0.2170-linux/bin/sonar-scanner
RUN echo '#/bin/bash' > /etc/init.d/sonar-scanner.sh
RUN echo 'export PATH="$PATH:/sonar-scanner-4.4.0.2170-linux/bin"' >> /etc/init.d/sonar-scanner.sh
RUN source /etc/init.d/sonar-scanner.sh
#RUN git clone https://vasilismets@bitbucket.org/vasilismets/citrix_ex.git
#WORKDIR /home/vmetax/citrix_ex
#RUN make
RUN /home/vmetax/sonar-scanner-4.4.0.2170-linux/bin/sonar-scanner -Dsonar.projectKey=citrix_build -Dsonar.sources=. -Dsonar.host.url=http://192.168.105.117:9000 -Dsonar.login=1b302df2a2da72dea6f17ce52587cdaabb1ffb8d -Dsonar.login=admin -Dsonar.password=admin
#CMD ["make"]
USER root
#CMD ["sh", "-c", "make && /home/vmetax/sonar-scanner-4.4.0.2170-linux/bin/sonar-scanner -Dsonar.projectKey=citrix_build -Dsonar.sources=. -Dsonar.host.url=http://192.168.105.117:9000 -Dsonar.login=1b302df2a2da72dea6f17ce52587cdaabb1ffb8d -Dsonar.login=admin -Dsonar.password=admin"]
